import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

    darkmode:boolean = false;

    constructor() { }

    ngOnInit(): void {
        let x = localStorage.getItem('DarkMode');
        if ( x === 'true') {
            this.darkmode = true;
            this.Modes();
        }
    }

    DarkMode() {
        this.darkmode = !this.darkmode;
        localStorage.setItem('DarkMode', this.darkmode.toString());
        this.Modes();
    }

    Modes() {
        let element = document.querySelector('body') as HTMLElement;
        let element1 = document.querySelector('.title') as HTMLElement;
         let element4 = document.querySelector('.navbar') as HTMLElement;




        if (localStorage.getItem('DarkMode')=== 'true' ) {
       
            element.classList.add('change-mode');
            element1.classList.add('change-nav');
            element4.classList.add('change-nav');

        }else {
let element2 = document.querySelector('.darkmode') as HTMLElement;
        let element3 = document.querySelector('.moon') as HTMLElement;
       
            element.classList.remove('change-mode');
            element1.classList.remove('change-nav');
            element4.classList.remove('change-nav');

        }


    }

}
