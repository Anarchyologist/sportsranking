import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { SharedataService } from '../../services/sharedata.service';

import { Store, select } from '@ngrx/store';
import { AppState } from '../../store/app.reducer';

import { takeUntil, mergeMap, switchMap , catchError, map, tap , delay , finalize } from 'rxjs/operators';
import { Observable, ReplaySubject, throwError , defer, of, forkJoin } from 'rxjs';

import { SportsGet } from '../../store/sports/sports.actions';

import { 
    selectAllSports,
} from '../../store/sports/sports.selector';

import { LeaguesGet } from '../../store/leagues/leagues.actions';

import { LeagueGet } from '../../store/league/league.actions';

import { 
    selectAllLeagues, 
    isLoading,
    selectLeague,
} from '../../store/leagues/leagues.selector';

import { 
    selLeague,
} from '../../store/league/league.selector';





@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    loading: boolean = true;    
    all_sports: Array<any> = [];
    all_leagues: Array<any> = [];
    leagues: Array<any> = [];

    search_val: string = "";

    sportControl = new FormControl();
    selectedSport: number = 0;
    sportFilteringCtrl = new FormControl();
    filteredSports: any;

    constructor(
        private share: SharedataService,
        private store: Store<AppState>,
        private route: Router,
    ) { }

    ngOnInit(): void {
        this.store.pipe(
            takeUntil(this.share.take_for),
            select(isLoading),
            map( res => {
                this.loading = res;
            }),
        ).subscribe();

        this.store.pipe(
            takeUntil(this.share.take_for),
            select(selectAllSports),
            map( (res) => {
                if (res.length === 0){
                    this.store.dispatch( new SportsGet());
                }else {
                    this.all_sports = res;
                    this.selectedSport = this.all_sports[0].idSport;
                    this.filteredSports = this.all_sports;
                };
            }),
        ).subscribe();


        let x = localStorage.getItem('selSport');

        if( !x ) {
            x = 'Soccer';
        }

        this.store.pipe(
            takeUntil(this.share.take_for),
            select(selectLeague(x)),
            map( (res) => {
                if(res.length === 0)
                    this.store.dispatch( new LeaguesGet());
                else {
                    this.leagues = res
                    this.all_leagues = this.leagues;
                };
            }),
        ).subscribe();

        this.sportFilteringCtrl.valueChanges
        .pipe(
            takeUntil(this.share.take_for),
            map((search:any) => {
                if (this.all_sports.length === 0) {
                    return [];
                }else {
                    return this.all_sports.filter((sport:any) => sport.strSport.toLowerCase().indexOf(search) > -1);
                }
            }),
        )
        .subscribe((filteredSports:any) => {
            this.filteredSports = filteredSports;
        });
    }

    public ngOnDestroy(): void {
        this.share.take_for.next(true);
        this.share.take_for.complete();
    }

    SelectSport() {
        let selSport = this.all_sports.filter( (sport:any) => sport.idSport === this.selectedSport);
        localStorage.setItem('selSport', selSport[0].strSport);
        let x = localStorage.getItem('selSport');
        if( x ) {
            this.store.pipe(
                takeUntil(this.share.take_for),
                select(selectLeague(x)),
                map( (res) => {
                    if(res.length === 0 ) {
                        this.store.dispatch( new LeaguesGet());
                    }else {
                        this.leagues = res;
                        this.all_leagues = this.leagues;
                    }
                }),
            ).subscribe();
        }
    }

    GoToRanking(id:number) {
        localStorage.setItem('LeagueId', id.toString());
        let x = localStorage.getItem('LeagueId');
        if(x){
            this.store.pipe(
                takeUntil(this.share.take_for),
                select(selLeague(x)),
                map( (res) => {
                    if(res.length === 0 ) {
                        this.store.dispatch( new LeagueGet());
                    }
                }),
            ).subscribe();
        }
    }

    NavigateTo(site:string) {
        window.location.host = site;
    }

    search(value: string) {
        this.leagues = this.all_leagues.filter((val) =>
            val.strLeague.toLowerCase().includes(value)
        );
    }

}
