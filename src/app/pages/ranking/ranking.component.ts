import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { SharedataService } from '../../services/sharedata.service';

import { Store, select } from '@ngrx/store';
import { AppState } from '../../store/app.reducer';

import { takeUntil, mergeMap, switchMap , catchError, map, tap , delay , finalize } from 'rxjs/operators';
import { Observable, ReplaySubject, throwError , defer, of, forkJoin } from 'rxjs';

import { 
    selectAllLeagues, 
    isLoading,
    selectLeague,
    getLeague
} from '../../store/leagues/leagues.selector';

import { LeagueGet } from '../../store/league/league.actions';
import { LeaguesGet } from '../../store/leagues/leagues.actions';

import { 
    selLeagueRank,
} from '../../store/league/league.selector';


@Component({
    selector: 'app-ranking',
    templateUrl: './ranking.component.html',
    styleUrls: ['./ranking.component.css']
})
export class RankingComponent implements OnInit {

    all_teams: Array<any> = [];
    league: any;

    constructor(
        private share: SharedataService,
        private store: Store<AppState>,

    ) { }

    ngOnInit(): void {
        let x = localStorage.getItem('LeagueId');
        if(x){
            this.store.pipe(
                takeUntil(this.share.take_for),
                select(getLeague(x)),
                map( (res) => {
                    if( res.length !== 0){
                        this.league = res[0];
                    }else{
                        this.store.dispatch( new LeaguesGet());
                    }
                }),
            ).subscribe();

            this.store.pipe(
                takeUntil(this.share.take_for),
                select(selLeagueRank(x)),
                map( (res) => {
                    if( res.length !== 0 ){
                        this.all_teams = res;
                    };

                }),
            ).subscribe();

            if(this.all_teams.length === 0){
                this.store.dispatch( new LeagueGet());
            };
        }
    }

    public ngOnDestroy(): void {
        this.share.take_for.next(true);
        this.share.take_for.complete();
    }
}

