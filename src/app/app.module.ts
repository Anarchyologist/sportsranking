//Initial
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';

//Material
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import {MatIconModule} from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';


//Components
import { HomeComponent } from './pages/home/home.component';
import { NavbarComponent } from './core/navbar/navbar.component';
import { FooterComponent } from './core/footer/footer.component';
import { RankingComponent } from './pages/ranking/ranking.component';

//Services
import { SharedataService } from './services/sharedata.service';

//Ngrx
import * as appReducer from './store/app.reducer';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';

//Ngrx specific
import { sportsReducer } from './store/sports/sports.reducer';
import { SportsEffects } from './store/sports/sports.effects';
import { leaguesReducer } from './store/leagues/leagues.reducer';
import { LeaguesEffects } from './store/leagues/leagues.effects';
import { leagueReducer } from './store/league/league.reducer';
import { LeagueEffects } from './store/league/league.effects';
import { SearchFilterPipe } from './pipes/search-filter.pipe';





@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        NavbarComponent,
        FooterComponent,
        RankingComponent,
        SearchFilterPipe
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NgbModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,

        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        NgxMatSelectSearchModule,
        MatIconModule,

        StoreModule.forRoot(appReducer.appReducer),
        EffectsModule.forRoot([]),
        StoreDevtoolsModule.instrument({ maxAge: 50, 
            logOnly: environment.production,
            autoPause: true,
        }),
        StoreModule.forFeature('sports', sportsReducer),
        EffectsModule.forFeature([SportsEffects]),
        StoreModule.forFeature('leagues', leaguesReducer),
        EffectsModule.forFeature([LeaguesEffects]),
        StoreModule.forFeature('league', leagueReducer),
        EffectsModule.forFeature([LeagueEffects]),


    ],
    providers: [
        SharedataService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
