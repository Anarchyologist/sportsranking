import { createFeatureSelector, createSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { SportsActions, SportsActionTypes } from './sports.actions';

export interface SportsState extends EntityState<any> {
    loading: boolean,
}

export const adapter: EntityAdapter<any> = createEntityAdapter<any>({
    selectId: (sports: any) => sports.idSport,
});

export const initialSportsState: SportsState = adapter.getInitialState( {
    loading: true,
});

export function sportsReducer( state = initialSportsState , action: SportsActions): SportsState {
    switch(action.type) {
        case SportsActionTypes.SportsUpsertMany: return adapter.upsertMany( action.payload.sports  , {
            ...state ,
            loading: false,
        });
        default: return state;
    }
}

export const {
    selectAll,
    selectEntities,
    selectIds,
    selectTotal,
} = adapter.getSelectors();
