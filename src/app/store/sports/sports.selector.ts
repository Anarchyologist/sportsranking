import { createFeatureSelector, createSelector } from '@ngrx/store';

import { SportsState, adapter } from './sports.reducer';

export const selectSportsState = createFeatureSelector<SportsState>('sports');

export const sportsSelectors = adapter.getSelectors();

export const selectAllSports = createSelector(
    selectSportsState,
    sportsState => { return Object.values(sportsState.entities); }
);

export const isLoading = createSelector(
    selectSportsState,
    sportsState => { return sportsState.loading; }
);

