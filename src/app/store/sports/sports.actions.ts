import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';

export enum SportsActionTypes {
        SportsGet = '[Sports] Get Available Sports',
        SportsUpsertMany = '[Sports] Update or Insert Sports',
        ActionFail = '[Sports API] Last Action Failed',
        ActionSuccess = '[Sports API] Last Action Success',
}

export class SportsGet implements Action {
    readonly type = SportsActionTypes.SportsGet;
}

export class SportsUpsertMany implements Action {
    readonly type = SportsActionTypes.SportsUpsertMany;
    constructor( public payload: { sports: Array<any> }) {}
}

export class ActionFail implements Action {
    readonly type = SportsActionTypes.ActionFail;
    constructor( public payload: { reason: string } ) {}
}

export class ActionSuccess implements Action {
    readonly type = SportsActionTypes.ActionSuccess;
    constructor( public payload: { reason: string } ) {}
}

export type SportsActions = SportsGet
    | SportsUpsertMany
    | ActionFail
    | ActionSuccess

