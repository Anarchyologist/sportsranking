import { Injectable } from '@angular/core';

import { mergeMap, switchMap , catchError, map, tap , delay , finalize } from 'rxjs/operators';
import { Observable, throwError , defer, of, forkJoin } from 'rxjs';

import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select, Action } from '@ngrx/store';

import { SharedataService } from '../../services/sharedata.service';

import { AppState } from '../app.reducer';

import {
    SportsActionTypes,
    ActionFail,
    ActionSuccess,
    SportsGet,
    SportsUpsertMany,
} from './sports.actions';

@Injectable()

export class SportsEffects {

    @Effect({ dispatch: false })
    success$ = this.actions$
        .pipe(
            ofType<ActionSuccess>(SportsActionTypes.ActionSuccess),
            map( action => action.payload),
            tap( (payload) => console.log(payload.reason) ),
        );

    @Effect()
    getSports$ = this.actions$
        .pipe(
            ofType<SportsGet>(SportsActionTypes.SportsGet),
            switchMap( (payload) => this.share.getSports().pipe(
                switchMap( (res:any) => [
                    new SportsUpsertMany({sports: res['sports']}),
                    new ActionSuccess({reason: 'Evala sports'}),
                ]),
                catchError( err => {
                    return [ new ActionFail({ reason: 'Den evala sports'}) ];
                }),
            )),
        );


 constructor(
        private actions$: Actions,
        private share: SharedataService,
        private store: Store<AppState>
    ) {}
}


