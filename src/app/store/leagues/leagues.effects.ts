import { Injectable } from '@angular/core';

import { mergeMap, switchMap , catchError, map, tap , delay , finalize } from 'rxjs/operators';

import { Observable, throwError , defer, of, forkJoin } from 'rxjs';

import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select, Action } from '@ngrx/store';

import { SharedataService } from '../../services/sharedata.service';

import { AppState } from '../app.reducer';

import {
    LeaguesActionTypes,
    ActionFail,
    ActionSuccess,
    LeaguesGet,
    LeaguesUpsertMany,
} from './leagues.actions';

@Injectable()

export class LeaguesEffects {

    @Effect({ dispatch: false })
    success$ = this.actions$
        .pipe(
            ofType<ActionSuccess>(LeaguesActionTypes.ActionSuccess),
            map( action => action.payload),
            tap( (payload) => console.log(payload.reason) ),
        );

    @Effect()
    getLeagues$ = this.actions$
        .pipe(
            ofType<LeaguesGet>(LeaguesActionTypes.LeaguesGet),
            switchMap( (payload) => this.share.getLeagues().pipe(
                switchMap( (res:any) => [
                    new LeaguesUpsertMany({leagues: res['countries']}),
                    new ActionSuccess({reason: 'Evala leagues'}),
                ]),
                catchError( err => {
                    return [ new ActionFail({ reason: 'Den evala leagues'}) ];
                }),
            )),
        );


 constructor(
        private actions$: Actions,
        private share: SharedataService,
        private store: Store<AppState>
    ) {}
}

