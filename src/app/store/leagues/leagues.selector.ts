import { createFeatureSelector, createSelector } from '@ngrx/store';

import { LeaguesState, adapter } from './leagues.reducer';

export const selectLeaguesState = createFeatureSelector<LeaguesState>('leagues');

export const leaguesSelectors = adapter.getSelectors();

export const selectAllLeagues = createSelector(
    selectLeaguesState,
    leaguesState => { return Object.values(leaguesState.entities); }
);

export const selectLeague = (sport: string) => createSelector(
    selectLeaguesState,
    leaguesState => { 
        return Object.values(leaguesState.entities).filter( x => x?.strSport === sport); 
    }
);

export const getLeague = (league: string) => createSelector(
    selectLeaguesState,
    leaguesState => {
        return Object.values(leaguesState.entities).filter( x => x?.idLeague === league);
    }
);

export const isLoading = createSelector(
    selectLeaguesState,
    leaguesState => { return leaguesState.loading; }
);
