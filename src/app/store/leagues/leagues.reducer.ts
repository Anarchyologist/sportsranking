import { createFeatureSelector, createSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { LeaguesActions, LeaguesActionTypes } from './leagues.actions';

export interface LeaguesState extends EntityState<any> {
    loading: boolean,
}

export const adapter: EntityAdapter<any> = createEntityAdapter<any>({
    selectId: (leagues: any) => leagues.idLeague,
});

export const initialLeaguesState: LeaguesState = adapter.getInitialState( {
    loading: true,
});

export function leaguesReducer( state = initialLeaguesState , action: LeaguesActions): LeaguesState {
    switch(action.type) {
        case LeaguesActionTypes.LeaguesUpsertMany: return adapter.upsertMany( action.payload.leagues  , {
            ...state ,
            loading: false,
        });
        default: return state;
    }
}

export const {
    selectAll,
    selectEntities,
    selectIds,
    selectTotal,
} = adapter.getSelectors();

