import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';

export enum LeaguesActionTypes {
        LeaguesGet = '[Leagues] Get Available Leagues',
        LeaguesUpsertMany = '[Leagues] Update or Insert Leagues',
        ActionFail = '[Leagues API] Last Action Failed',
        ActionSuccess = '[Leagues API] Last Action Success',
}

export class LeaguesGet implements Action {
    readonly type = LeaguesActionTypes.LeaguesGet;
}

export class LeaguesUpsertMany implements Action {
    readonly type = LeaguesActionTypes.LeaguesUpsertMany;
    constructor( public payload: { leagues: Array<any> }) {}
}

export class ActionFail implements Action {
    readonly type = LeaguesActionTypes.ActionFail;
    constructor( public payload: { reason: string } ) {}
}

export class ActionSuccess implements Action {
    readonly type = LeaguesActionTypes.ActionSuccess;
    constructor( public payload: { reason: string } ) {}
}

export type LeaguesActions = LeaguesGet
    | LeaguesUpsertMany
    | ActionFail
    | ActionSuccess

