import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';

export enum LeagueActionTypes {
        LeagueGet = '[League] Get Available League',
        LeagueUpsertMany = '[League] Update or Insert League',
        ActionFail = '[League API] Last Action Failed',
        ActionSuccess = '[League API] Last Action Success',
}

export class LeagueGet implements Action {
    readonly type = LeagueActionTypes.LeagueGet;
}

export class LeagueUpsertMany implements Action {
    readonly type = LeagueActionTypes.LeagueUpsertMany;
    constructor( public payload: { league: Array<any> }) {}
}

export class ActionFail implements Action {
    readonly type = LeagueActionTypes.ActionFail;
    constructor( public payload: { reason: string } ) {}
}

export class ActionSuccess implements Action {
    readonly type = LeagueActionTypes.ActionSuccess;
    constructor( public payload: { reason: string } ) {}
}

export type LeagueActions = LeagueGet
    | LeagueUpsertMany
    | ActionFail
    | ActionSuccess

