import { createFeatureSelector, createSelector } from '@ngrx/store';

import { LeagueState, adapter } from './league.reducer';

export const selectLeagueState = createFeatureSelector<LeagueState>('league');

export const leagueSelectors = adapter.getSelectors();

export const selectAllLeague = createSelector(
    selectLeagueState,
    leagueState => { return Object.values(leagueState.entities); }
);

export const selLeague = (league: string) => createSelector(
    selectLeagueState,
    leagueState => { 
        return Object.values(leagueState.entities).filter( x => x?.idLeague === league); 
    }
);

export const selLeagueRank = (league: string) => createSelector(
    selectLeagueState,
    leagueState => { 
        let x = Object.values(leagueState.entities).filter( x => x?.idLeague === league); 
        return x.sort((a,b)=> a.intRank - b.intRank);
    }
);

export const isLoading = createSelector(
    selectLeagueState,
    leagueState => { return leagueState.loading; }
);

