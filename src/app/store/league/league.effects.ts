import { Injectable } from '@angular/core';

import { mergeMap, switchMap , catchError, map, tap , delay , finalize } from 'rxjs/operators';

import { Observable, throwError , defer, of, forkJoin } from 'rxjs';

import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select, Action } from '@ngrx/store';

import { SharedataService } from '../../services/sharedata.service';

import { AppState } from '../app.reducer';

import {
    LeagueActionTypes,
    ActionFail,
    ActionSuccess,
    LeagueGet,
    LeagueUpsertMany,
} from './league.actions';

@Injectable()

export class LeagueEffects {

    @Effect({ dispatch: false })
    success$ = this.actions$
        .pipe(
            ofType<ActionSuccess>(LeagueActionTypes.ActionSuccess),
            map( action => action.payload),
            tap( (payload) => console.log(payload.reason) ),
        );

    @Effect()
    getLeague$ = this.actions$
        .pipe(
            ofType<LeagueGet>(LeagueActionTypes.LeagueGet),
            switchMap( (payload) => this.share.getLeague().pipe(
                switchMap( (res:any) => [
                    new LeagueUpsertMany({league: res['table']}),
                    new ActionSuccess({reason: 'Evala league'}),
                ]),
                catchError( err => {
                    return [ new ActionFail({ reason: 'Den evala league'}) ];
                }),
            )),
        );


 constructor(
        private actions$: Actions,
        private share: SharedataService,
        private store: Store<AppState>
    ) {}
}

