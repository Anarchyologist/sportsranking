import { createFeatureSelector, createSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { LeagueActions, LeagueActionTypes } from './league.actions';

export interface LeagueState extends EntityState<any> {
    loading: boolean,
}

export const adapter: EntityAdapter<any> = createEntityAdapter<any>({
    selectId: (league: any) => league.idTeam,
});

export const initialLeagueState: LeagueState = adapter.getInitialState( {
    loading: true,
});

export function leagueReducer( state = initialLeagueState , action: LeagueActions): LeagueState {
    switch(action.type) {
        case LeagueActionTypes.LeagueUpsertMany: return adapter.upsertMany( action.payload.league  , {
            ...state ,
            loading: false,
        });
        default: return state;
    }
}

export const {
    selectAll,
    selectEntities,
    selectIds,
    selectTotal,
} = adapter.getSelectors();

