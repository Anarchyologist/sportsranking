import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';

//import { Store, select } from '@ngrx/store';
//import { AppState } from '../../../store/app.reducer';

import { skip, mergeMap, switchMap , catchError, map, tap , delay , finalize } from 'rxjs/operators';
import { Observable, Subject, throwError , defer, of, forkJoin, timer } from 'rxjs';


@Injectable()
export class SharedataService {

    take_for = new Subject<boolean>();

    constructor(
        private http: HttpClient,
        //private store: Store<AppState>,
    ) { }

    getSports(): Observable<any[]> {
        return this.http.get<any[]>(
            `https://www.thesportsdb.com/api/v1/json/2/all_sports.php`,
        );
    }

    getLeagues(): Observable<any[]> {
        let x = localStorage.getItem('selSport');
        if(!x) {
            x = 'Soccer';
        };
        return this.http.get<any[]>(
            `https://www.thesportsdb.com/api/v1/json/2/search_all_leagues.php?s=${x}`,
        );
    }

    getLeague(): Observable<any[]> {
        return this.http.get<any[]>(
            `https://www.thesportsdb.com/api/v1/json/2/lookuptable.php?l=${localStorage.getItem('LeagueId')}&s=2021-2022`,
        );
    }



}

